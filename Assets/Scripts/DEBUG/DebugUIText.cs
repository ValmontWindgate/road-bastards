﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugUIText : MonoBehaviour {

	public Text[] texts;

	// Use this for initialization
	void Start () {
		texts = GameObject.FindObjectsOfType<Text>( );
	}
	
	// Update is called once per frame
	void Update () {
		int index = Random.Range (0,texts.Length);
		if ( texts [ index ] != null ) {
			if ( texts [ index ].gameObject.name == "Text Ammount" ) {
				texts [ index ].text = "" + Random.Range ( 0f , 1000000f );
			}
		}
	}
}
