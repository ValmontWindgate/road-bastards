﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtMainCamera : StateMachineBehaviour {
	
	public bool ready;
	public Vector3 target;

	public override void OnStateIK ( Animator animator , AnimatorStateInfo info , int layer ) {
		if ( !ready ) {
			ready = true;
			target = animator.transform.position + animator.transform.forward + animator.transform.up*3;
		}
		target = Vector3.Slerp ( target , Camera.main.transform.position , 0.1f );
		animator.SetLookAtPosition ( target );
		animator.SetLookAtWeight ( 1 );
	}

}
