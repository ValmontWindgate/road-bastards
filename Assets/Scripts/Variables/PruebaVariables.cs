﻿using UnityEngine;

public class PruebaVariables : MonoBehaviour { 

	// Variables primitivas
	public int granadas;
	public float probabilidadCritico;
	public string nombre;
	public bool muerto;

	// Variables estructura
	public Vector3 spawner;
	public Color colorPelo;

	// Variables lista de selección
	public KeyCode teclaDisparo;
	public Space espacio;
	public ForceMode modoFuerza;

	

}
