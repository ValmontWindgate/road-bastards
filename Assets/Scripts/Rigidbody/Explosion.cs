﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public GameObject particles;
	public float radius;
	public float power;
	
	void Update ( ) {
		if ( !Input.GetKeyDown ( KeyCode.E ) ) return;
		Explode ( );
	}

	void Explode () {
		Collider [ ] colliders = Physics.OverlapSphere ( transform.position , radius );
		foreach ( Collider current in colliders ) {
			Animator tempAnimator = current.GetComponent<Animator>( );
			if ( tempAnimator ) Destroy ( tempAnimator );
			Rigidbody tempRigidbody = current.GetComponent<Rigidbody>( );
			if ( !tempRigidbody ) continue;
			tempRigidbody.velocity = Vector3.zero;
			tempRigidbody.angularVelocity = Vector3.zero;
			tempRigidbody.AddExplosionForce ( power , transform.position , radius , 2 , ForceMode.Impulse );
		}
		Instantiate ( particles , transform.position , transform.rotation );
		Destroy ( this );
	}

}
