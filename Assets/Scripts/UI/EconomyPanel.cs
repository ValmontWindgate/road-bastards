﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EconomyPanel : MonoBehaviour {

	public Text textRations, textMaterials, textSteel, textGold, textPopulation;

	void Start ( ) {
		InvokeRepeating ( "UpdateUI" , 1f , 1f );
	}

	public void UpdateUI ( ) {
		Debug.Log (" ANO");
		textRations.text = "" + (float)Rations.total;
		textMaterials.text = "" + (float)Materials.total;
		textSteel.text = "" + (float)Steel.total;
		textGold.text = "" + (float)Gold.total;
		textPopulation.text = (float)Population.total + " WORKERS\n" + (float)Population.total + "/DAY";
	}

}
