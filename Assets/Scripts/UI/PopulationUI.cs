﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopulationUI : MonoBehaviour {

	public Text foodProducersText , recyclersText , metalWorkersText , buildersText ,
		medicalsText , fuelProducersText , 	ammoProducersText ,
		wheelEngineersText , depositEngineersText , enginesEngineersText , seatsEngineersText , cabinesEngineersText ,
		soldiersText , pilotsText;
	
	void Start ( ) {
		InvokeRepeating ( "UpdateUI" , 1 , 1 );
	}

	void UpdateUI ( ) {
		foodProducersText.text = "Food Producers\n" + Workers.rationsWorkers;
		recyclersText.text = "Recyclers\n" + Workers.materialsWorkers;
		metalWorkersText.text = "Metal Workers\n" + Workers.steelWorkers;
		buildersText.text = "Builders\n" + Workers.buildingWorkers;
		
		medicalsText.text = "Food Producers\n" + Officials.medkitsOfficials;
		fuelProducersText.text = "Food Producers\n" + Officials.fuelOfficials;
		ammoProducersText.text = "Food Producers\n" + Officials.ammoOfficials;

		wheelEngineersText.text = "Food Producers\n" + Engineers.wheelsEngineers;
		depositEngineersText.text = "Food Producers\n" + Engineers.depositsEngineers;
		enginesEngineersText.text = "Food Producers\n" + Engineers.enginesEngineers;
		seatsEngineersText.text = "Food Producers\n" + Engineers.seatEngineers;
		cabinesEngineersText.text = "Food Producers\n" + Engineers.cabineEngineers;

		soldiersText.text = "Food Producers\n" + Soldiers.activeSoldiers;
		pilotsText.text = "Food Producers\n" + Pilots.totalPilots;
	}

}
