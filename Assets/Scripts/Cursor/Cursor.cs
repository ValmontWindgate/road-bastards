﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour {

	private MeshRenderer renderer;
	public float speed = 5f;
	public bool isMoving;
	public enum Mode { Move , Selection , Destroy , Other };
	private Mode mode = Mode.Move;
	public List<Material> cursorMaterials;
	public GameObject cursorSelectionObject;
	
	void Start () {
		renderer = GetComponentInChildren<MeshRenderer>( );
		cursorSelectionObject.SetActive ( false );
	}

	public Mode ColorTint {
		get { return this.mode; }
		set { 
			this.mode = value;
			this.renderer.material = this.cursorMaterials [ (int)value ];
		} 
	}

	public IEnumerator Move ( Vector3 direction ) {
		isMoving = true;
		Vector3 move = Vector3.zero;
		while ( move.magnitude < 1f ) {
			move += direction * speed * Time.deltaTime;
			transform.Translate ( direction * speed * Time.deltaTime );
			yield return null;
		}
		transform.Translate ( direction * ( 1f - move.magnitude ) );
		transform.position = new Vector3 ( Mathf.RoundToInt(transform.position.x) , Mathf.RoundToInt(transform.position.y) , Mathf.RoundToInt(transform.position.z) );
		isMoving = false;
	}

	public void SetPosition ( Vector3 position ) {
		transform.position = position;
	}

	// Activate/desactivate the selection cursor. If activates, also bring it to current cursor position
	public void ShowSelectionCursor ( bool show ) {
		cursorSelectionObject.SetActive ( show );
		if ( show ) cursorSelectionObject.transform.position = transform.position;
	}

}
