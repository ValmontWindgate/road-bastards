﻿using System;
using UnityEngine;

public interface IRecursos {
	int PesoUnitario ( );
	int Cantidad ( );
	int PesoTotal ( );
}

[System.Serializable]
public class Comida : IRecursos {
	public int cantidad = 1000;
	public int PesoUnitario ( ) {
		return 1;
	}
	public int Cantidad ( ) {
		return cantidad;
	}
	public int PesoTotal ( ) {
		return PesoUnitario ( ) * Cantidad ( );
	}
}

[System.Serializable]
public class Madera : IRecursos {
	public int cantidad = 100;
	public int PesoUnitario ( ) {
		return 5;
	}
	public int Cantidad ( ) {
		return cantidad;
	}
	public int PesoTotal ( ) {
		return PesoUnitario ( ) * Cantidad ( );
	}
}

public class InterfazAccesoInformacion : MonoBehaviour {
	[SerializeField] public Comida comida;
	[SerializeField] public Madera madera;
	[SerializeField] public IRecursos [ ] recursos;
	void Start ( ) {
		comida = new Comida ( );
		madera = new Madera ( );
		recursos = new IRecursos [ 2 ]; 
		recursos [ 0 ] = comida;
		recursos [ 1 ] = madera;
	}
	void Update ( ) {
		if ( Input.GetKeyDown ( KeyCode.C ) ) comida.Cantidad ( );
		if ( Input.GetKeyDown ( KeyCode.M ) ) madera.Cantidad ( );
		Debug.Log ( "Peso total " + recursos[0].PesoTotal() + recursos[1].PesoTotal() );
	}
}