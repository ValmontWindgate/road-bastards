﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponDB {
    public int ammo = 100 , burst;
    public float dmgMin = 1 , dmgMax = 10;
    public float desv;
}

[System.Serializable]
public class CharacterDB {
    public string name;
    public float hp = 100 , hpMax = 100;
    public WeaponDB weapon;
}

[System.Serializable]
public class ModuleDB {
    public enum Type { Pilot , Wheel , Engine , Deposit , Seat , Armor , Scooter , Load , Turret , Anchor };
    public Type type; 
    public float hp = 100 , hpMax = 100;
    public float weight = 100;
}

[System.Serializable]
public class PilotDB : ModuleDB {
    public int steer = 0 , steerMax = 1;
    public int gear = 0 , gearMax = 1;
    public CharacterDB character;
    public bool rearview;
    public PilotDB ( ) {
        type = ModuleDB.Type.Pilot;
    }
}

[System.Serializable]
public class WheelDB : ModuleDB {
    public bool isDrive , isSteer;
    public WheelDB ( ) {
        type = ModuleDB.Type.Wheel;
    }
}

[System.Serializable]
public class EngineDB : ModuleDB {
    public float power = 1000;
    public EngineDB ( ) {
        type = ModuleDB.Type.Engine;
    }
}

[System.Serializable]
public class DepositDB : ModuleDB {
    public float fuel = 10000 , fuelMax = 10000;
    public DepositDB ( ) {
        type = ModuleDB.Type.Deposit;
    }
}

[System.Serializable]
public class SeatDB : ModuleDB {
    public CharacterDB character;
    public bool rearview;
    public SeatDB ( ) {
        type = ModuleDB.Type.Seat;
    }
}

[System.Serializable]
public class ArmorDB : ModuleDB {
    public float shield = 100 , shieldMax = 100;
    public bool scope = false;
}

[System.Serializable]
public class ScooterDB : ModuleDB {
    public int steer = 0 , steerMax = 1;
    public int gear = 0 , gearMax = 1;
    public float power = 600;
    public float fuel = 6000, maxFuel = 6000;
}

// load, turret, anchor, 