﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothViewportChange : MonoBehaviour {

	public List<Camera> cameras;
	public int currentIndex = 0;
	public Camera current, last;
	public bool change , next;
	public bool isChanging = false;

	void Start ( ) {
		current = cameras [ 0 ];
		current.tag = "MainCamera";
		last = cameras [ cameras.Count-1 ];
		for ( int i = 1 ; i < cameras.Count ; i++ ) cameras [ i ].rect = new Rect ( 0 , 0 , 0 , 1 );
	}
	
	void Update () {
		if ( change && !isChanging ) {
			change = false;	// Resets and avoids another change to exec
			isChanging = true;
			StartCoroutine ( Change ( ) );
		}
	}

	public void ChangeButton ( bool isNext ) {	// Sets change to true if not is changing now
		if ( !isChanging ) {
			change = true;
			next = isNext;
		}
	}

	IEnumerator Change ( ) {
		
		last = cameras [ currentIndex ];	// Sets the last and next camera
		last.depth = 0;
		if ( next ) currentIndex++;
		else currentIndex--;
		if ( currentIndex >= cameras.Count ) currentIndex = 0;
		if ( currentIndex < 0 ) currentIndex = cameras.Count-1;
		current = cameras [ currentIndex ];
		current.depth = 1;

		float size = 0f;
		while ( current.rect.width < 1 ) {
			size += Time.deltaTime;
			size = Mathf.Clamp ( size , 0 , 1 );
			if ( next ) current.rect = new Rect ( 0 , 0 , size , 1f );
			else current.rect = new Rect ( 1f-size , 0 , size , 1f );
			yield return null;
		}
		last.rect = new Rect ( 0 , 0 , 0 , 1 );

		last.tag = "Untagged";
		current.tag = "MainCamera";

		next = false;
		isChanging = false;
	}

}
