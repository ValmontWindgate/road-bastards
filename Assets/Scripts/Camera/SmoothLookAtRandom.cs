﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothLookAtRandom : MonoBehaviour {

	public float smooth = 0.01f;
	public float changeTime = 20f;
	private float timer = 0f;
	private GameObject target;
	private MeshRenderer [ ] renderers;
	private Vector3 forward;
	public List<GameObject> buildingCameraPivots;
	public bool lookingBuilding = false;
	
	void Start ( ) {
		if ( renderers == null ) renderers = FindObjectsOfType<MeshRenderer>( );
		Change ( );
	}

	void Update ( ) {
		transform.forward = Vector3.Slerp ( transform.forward , forward.normalized , smooth );
		if ( lookingBuilding ) {
			transform.position = Vector3.Slerp ( transform.position , target.transform.position , smooth );
		}
		else {
			Vector3 move = new Vector3 ( transform.forward.x , 0 , transform.forward.z );
			transform.Translate ( move.normalized * Time.deltaTime , Space.World );
		}
		
		timer += Time.deltaTime;
		if ( timer < changeTime ) return;
		timer = 0;
		Change ( );
	}

	void Change ( ) {
		lookingBuilding = false;
		target = renderers [ Random.Range ( 0 , renderers.Length ) ].gameObject;
		forward = target.transform.position - transform.position;
	}

	public void LookAt ( int index ) {
		lookingBuilding = true;
		target = buildingCameraPivots [ 0 ].gameObject;
		forward = buildingCameraPivots [ 0 ].transform.forward;
		timer = 0f;
	}

}
