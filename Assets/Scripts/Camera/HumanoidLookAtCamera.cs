﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanoidLookAtCamera : MonoBehaviour {
	
	// Doesn't work with these humanoids :(
	void OnAnimatorIK ( ) {
		GetComponent<Animator>( ).SetLookAtPosition ( Camera.main.transform.position );
	}

}
