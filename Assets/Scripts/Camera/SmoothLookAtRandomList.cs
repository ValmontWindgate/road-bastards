﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothLookAtRandomList : MonoBehaviour {

	public float smooth = 0.005f;
	public float changeTime = 5f;
	private float time;
	private int current;
	public GameObject [ ] targets;
	private Vector3 forward;
	
	void Start ( ) {
		if ( targets.Length == 0 ) Debug.LogError ( "No targets to look in SmoothLookAtRandomList.cs" );
		StartCoroutine ( ChangeCoroutine ( ) ); 
	}

	void Update ( ) {
		transform.forward = Vector3.Slerp ( transform.forward , forward.normalized , smooth );
		if ( Input.GetMouseButtonDown ( 0 ) || ( Input.touchCount == 1 && Input.touches[0].phase==TouchPhase.Began)) {
			Next ( );
		}
	}

	public IEnumerator ChangeCoroutine ( ) {
		while ( true ) {
			time += Time.deltaTime;
			if ( time > changeTime ) {
				Change ( );
			}
			yield return null;
		}
	}

	public void Change ( ) {
		time = 0f;
		current = Random.Range ( 0 , targets.Length );
		forward = ( targets [current].transform.position+2*Vector3.up) - transform.position;
	}

	public void Next ( ) {
		time = 0f;
		current++;
		if ( current >= targets.Length ) current = 0;
		forward = ( targets[current].transform.position+2*Vector3.up) - transform.position;
	}

}
