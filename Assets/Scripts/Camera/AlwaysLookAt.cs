﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlwaysLookAt : MonoBehaviour {

	private Vector3 difference;
	public GameObject objective;
	public float smooth = 0.05f;
	
	void Update () {
		difference = objective.transform.position - transform.position;
		transform.forward = Vector3.Lerp ( transform.forward , difference , smooth );
	}

}
