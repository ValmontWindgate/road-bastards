﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDestroy : MonoBehaviour {

	void OnTriggerEnter ( Collider info ) {
		Destroy ( info.gameObject );
	}

}
