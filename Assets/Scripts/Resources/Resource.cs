﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Resource : MonoBehaviour {

   public ResourceData resourceData;

   public float totalRaw;
   public int total { get { return (int)totalRaw; } }
   public float progress { get { return totalRaw - total; } }

}
