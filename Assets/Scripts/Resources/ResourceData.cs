﻿using UnityEngine;
using System.Collections.Generic;
using RoadBastards;

[CreateAssetMenu ( menuName = "Road Bastards/Resource") ]
public class ResourceData : ScriptableObject {

	public string name;										// Name of the resource
	public Sprite sprite;									// UI and 3D aspect
	public GameObject prefab;	
	public float unitWeight;								// For trading... TODO	
	public CitizenData requiredCitizen;	
	
	/*public ResourceType type;								// Type of resource, range, worker range needed, associated work type and production building
	public ResourceRangeType range;						
	public CitizenRangeType requiredWorkRange;
	public WorkType associatedWorkType;
	public BuildingType associatedProductionBuilding;
	
	// Weight per unit and production for each assigned worker in an hour
	public float baseProductionPerWorkerHour;
	public float baseProductionPerWorker { get { return baseProductionPerWorkerHour / 3600f;} }

	[SerializeField]
	public List<ResourceIncome> outcomeResources;			// Resources consumed each hour for each worker (negative incoming) for keeping this resource production */

}
