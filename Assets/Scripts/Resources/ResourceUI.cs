﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceUI : MonoBehaviour
{
    public Text nameText;
    public Text progressText;
    public Image spriteImage;
    public Text ammountText;
    public Slider slider;
    public Image sliderFill;

    public void UpdateUI ( string name , Sprite sprite , int amount , int progress , int max ) {
        nameText.text = name;
        progressText.text = "Next " + progress + "%";
        spriteImage.sprite = sprite;
        ammountText.text = amount + ( ( max < 1000000000 && max >= 0) ? "/"+max :"" );
        slider.value = ( ( max < 1000000000 && max >= 0) ? (float)amount/(float)max : 1 );
        sliderFill.color = new Color ( 1/slider.value , slider.value , 0 );
    }

}
