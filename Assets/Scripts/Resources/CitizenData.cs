using UnityEngine;
using System.Collections.Generic;
using RoadBastards;

[CreateAssetMenu ( menuName = "Road Bastards/Citizen") ]
public class CitizenData : ResourceData {
    // i.e worker eat ration, worker give gold and pilot use fuel
    [SerializeField]
    public ResourceIncomeBundle passiveIncomePerUnit;   
}