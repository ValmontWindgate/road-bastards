﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResourceProductionUI : MonoBehaviour {

    public Text nameText;
    public Image spriteImage;
    public Text ammountText;
    public Slider sliderAmmount;
    public Text progressText;
    public Slider sliderProgress;

    public Slider sliderIdle;
    public Text idleText;
    public Slider sliderActive;
    public Text activeText;

    public Button button;
    public Text buttonText;

    public void UpdateUI ( Resource resource ) {

        nameText.text = resource.resourceData.name;
        spriteImage.sprite = resource.resourceData.sprite;

        // Show stored values, max storage (if exists), income and next unit progress
        float totalRaw = ResourcesControl.main.GetResourceAmount ( resource.resourceData );
        int storageLimit = (int)BuildingsControl.main.GetStorageLimit ( resource.resourceData );
        ammountText.text = (int)totalRaw + ( storageLimit<1000000&&storageLimit>0 ? "/" + storageLimit : "");
        sliderAmmount.value = (float)totalRaw/(float)storageLimit;

        progressText.text =  "+" + BuildingsControl.main.GetGlobalProductionIncome ( resource.resourceData ) + "/h (" + 
            (int)(100*(totalRaw - (int)totalRaw)) + "%)";
        sliderProgress.value = (totalRaw - (int)totalRaw);

        // Set slider for idle citizens
        sliderIdle.value = 1f;
        int idleCitizens = CitizensControl.main.GetIdleCitizens ( resource.resourceData.requiredCitizen );
        idleText.text = "Idle citizens " + idleCitizens;

        // Set slider for active citizens VS max citizens allowed
        int activeCitizens = BuildingsControl.main.GetActiveCitizensInResource ( resource.resourceData );
        int maxCitizens = 1000000000;
        ProductionBuilding productionBuilding = BuildingsControl.main.GetBuildingForResource ( resource.resourceData );
        if ( productionBuilding != null ) maxCitizens = productionBuilding.maxCitizens;
        else maxCitizens = BuildingsControl.main.residencesBuilding.maxCitizens;
        sliderActive.value = ( maxCitizens<1000000&&maxCitizens>0 ? (float)((float)activeCitizens/(float)maxCitizens) : 1f );
        activeText.text = activeCitizens + ( maxCitizens<1000000&&maxCitizens>0 ? "/" + maxCitizens : "");

        // Sets the button to move idle citizens to this production building, only for production buildings
        if ( idleCitizens > 0 && productionBuilding != null ) {
            buttonText.gameObject.SetActive ( true );
            button.interactable = true;
            button.onClick.RemoveAllListeners ( );
            button.onClick.AddListener ( ( ) => Button ( resource ) );
        }
        else {
            buttonText.gameObject.SetActive ( false );
            button.interactable = false;
        }

    }

    public void Button ( Resource resource ) {

        int idleCitizensToMove = CitizensControl.main.GetIdleCitizens ( resource.resourceData.requiredCitizen );

        int availableCitizenSlots;
        int activeCitizens = BuildingsControl.main.GetActiveCitizensInResource ( resource.resourceData );
        int maxCitizens = 1000000000;
        ProductionBuilding productionBuilding = BuildingsControl.main.GetBuildingForResource ( resource.resourceData );
        if ( productionBuilding != null ) maxCitizens = productionBuilding.maxCitizens;
        else productionBuilding = BuildingsControl.main.GetBuildingBuilding ( );
        availableCitizenSlots = maxCitizens - activeCitizens;

        if ( idleCitizensToMove > availableCitizenSlots ) idleCitizensToMove = availableCitizenSlots;

        productionBuilding.citizens += idleCitizensToMove;
        // Idle citizens auto refresh ???

    }
   
}
