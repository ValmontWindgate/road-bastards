﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProductionUI : MonoBehaviour
{
    public Text nameText;
    public Text incomeText;
    public Image spriteImage;
    public Text ammountText;
    public Slider slider;
    public Image sliderFill;

    public void UpdateUI ( string name , Sprite sprite , int income , int amount , int max ) {
        nameText.text = name;
        incomeText.text = income != 0 ? "Income " + income + "/h" : "";
        spriteImage.sprite = sprite;
        ammountText.text = amount + ( ( max < 1000000000 && max >= 0) ? "/"+max :"" );
        slider.value = ( ( max < 1000000000 && max >= 0) ? (float)amount/(float)max : 1 );
        sliderFill.color = new Color ( 1/slider.value , slider.value , 0 );
    }

}
