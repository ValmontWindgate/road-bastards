﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gold : Resource {

	public static string name = "Gold";
	public static readonly float weight = 1f;

	public static float total = 0;

	public static int workers = 4;
	public static float incomeSec = 1f/60f/60f/24f;
	public static float IncomeHour {
		get { return incomeSec*60f*60f; }
		set { }
	}

}
