﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Population : Resource {

	public static string name = "Population";

	public static float total = 4f;
	public static int totalWorkers = 4;
	public static int totalOfficials, totalEngineers, totalSoldiers, totalPilots;
	public static float totalMax = 20f;

	public static float incomeSec = 1f/60f/60f;
	public static float IncomeHour {
		get { return incomeSec*60f*60f; }
		set { }
	}

}
