﻿public class Steel : Resource {

	public static string name = "Steel";
	public static readonly float weight = 4f;

	public static float total = 1f;
	public static float totalMax = 1000f;
	
	public static int workers = 1;
	public static float incomeSec = 1f/60f/60f;
	public static float IncomeHour {
		get { return incomeSec*60f*60f; }
		set { }
	}

}
