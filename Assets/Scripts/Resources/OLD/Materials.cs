﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Materials : Resource {

	public static string name = "Materials";
	public static readonly float weight = 3f;

	public static float total = 10f;
	public static float totalMax = 1000f;
	
	public static int workers = 1;
	public static float incomeSec = 2f/60f/60f;
	public static float IncomeHour {
		get { return incomeSec*60f*60f; }
		set { }
	}

}
