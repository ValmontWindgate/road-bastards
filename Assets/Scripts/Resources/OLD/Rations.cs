﻿public class Rations : Resource {

	public static string name = "Rations";
	public static readonly float weight = 2f;

	public static float total = 100f;
	public static float totalMax = 1000f;
	
	public static int workers = 1;
	public static float incomeSec = 4f/60f/60f;
	public static float IncomeHour {
		get { return incomeSec*60f*60f; }
		set { }
	}

}
