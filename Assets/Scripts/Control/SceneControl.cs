﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneControl : MonoBehaviour {

	public static SceneControl main;

	void Awake ( ) {
		main = this;
	}
	
	void Update ( ) {
		if ( Input.GetKeyDown ( KeyCode.Escape ) ) {
			if ( SceneManager.GetActiveScene( ).buildIndex != 0 ) LoadScene ( 0 );
		}
	}

	public void LoadScene ( int scene ) {
		StartCoroutine ( LoadSceneAsync ( scene ) );
	}

	public IEnumerator LoadSceneAsync ( int scene ) {
		AsyncOperation load = SceneManager.LoadSceneAsync ( scene );
		while ( !load.isDone ) {
			Debug.Log ( "LOADING " + (int)(100f*load.progress) + "%" );
			yield return null;
		}
	}

	public void QuitGame ( ) {
		Application.Quit ( );
	}

}
