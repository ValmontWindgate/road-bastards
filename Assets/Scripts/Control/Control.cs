﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Control : MonoBehaviour {

	public static Control main;
	private float resetTimer = 0f;

	void Awake () {
		if ( Application.internetReachability == NetworkReachability.NotReachable ) {
            Debug.Log ( "Error. Check internet connection!" );
			Application.Quit ( );
        }
		main = this;
		Application.targetFrameRate = 24;
	}

	IEnumerator Start ( ) {

		// Debug info
		CanvasControl.main.DebugLog ( "FPS " + (int)(1f/Time.deltaTime) + "/" + Application.targetFrameRate );
		CanvasControl.main.DebugLog ( "Scene " + Application.loadedLevelName + " running on " + Application.platform.ToString ( ) );

		// Fade
		CanvasControl.main.Show ( CanvasControl.TpCanvas.Fade );
		CanvasControl.main.Fade ( new Color (0,0,0,0) , 5f );
		while ( CanvasControl.main.IsFading ( ) ) yield return null;

		// DB connection and get timestamps
		while ( DBControl.main.busy ) yield return null;
		CanvasControl.main.DebugLog ( "Database time " + DBControl.currentDateTime );
		CanvasControl.main.DebugLog ( "Device time " + System.DateTime.Now.ToUniversalTime ( ) );
		
	}
	

	void Update () {
		if ( Input.GetKeyDown ( KeyCode.B ) ) {
			BuildingDB building = new FoodProductionPlantDB ( );
			FoodProductionPlantDB.level = Random.Range ( 1 , 20 );
			Debug.Log ( "Building cost: " + building.goldCost + " at level " + FoodProductionPlantDB.level );
		}
		// Reset game and restart it by pressing Esc or R button for 5 sec
		if ( Input.GetKey ( KeyCode.R ) || Input.GetKey ( KeyCode.Escape ) ) {
			resetTimer += Time.deltaTime;
			if ( resetTimer > 5f ) {
				resetTimer = 0f;
				FindObjectOfType<SaveLoadControl>( ).ResetData ( );
				 // DBControl.main.currentTime = 0;
				StartCoroutine ( SetResetTime ( ) );
			}
		}
	}

	public IEnumerator SetResetTime ( ) {
		double now = 0;
		yield return DBControl.GetTime ( value => now = value );
		PlayerPrefs.SetFloat ( "Last Time" , (float)now );
		yield return null;
		SceneControl.main.LoadScene ( 0 );
	}

	public void CloseGame ( ) {
		Application.Quit ( );
	}

}
