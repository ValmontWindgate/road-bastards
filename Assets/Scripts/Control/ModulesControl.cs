﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModulesControl : MonoBehaviour {

	public static ModulesControl main;

	// This is for the always visible UI of resources
	public Text textWheels, textDeposits, textEngines, textSeats, textCabines;

	void Awake ( ) {
		main = this;
	}

	void Start ( ) {
		// InvokeRepeating ( "ProcessIncome" , 1f , 1f );	// NOT USED CAUSE INCOME IS PROCESSED ONLY AT START
		StartCoroutine ( CalculateDisconnectedIncome ( ) );
	}

	public IEnumerator CalculateDisconnectedIncome ( ) {

		while ( DBControl.main.elapsedTimeSinceLastCheck == 0 ) yield return null;		// Waits DB response

		// Calculates incoming for each module
		/*
		Wheels.total += (float)( Wheels.incomeSec * DBControl.main.elapsedTimeSinceLastCheck ) * Engineers.wheelsEngineers;
		Deposits.total += (float)( Deposits.incomeSec * DBControl.main.elapsedTimeSinceLastCheck ) * Engineers.depositsEngineers;
		Engines.total += (float)( Engines.incomeSec * DBControl.main.elapsedTimeSinceLastCheck ) * Engineers.enginesEngineers;
		Seats.total += (float)( Seats.incomeSec * DBControl.main.elapsedTimeSinceLastCheck ) * Engineers.seatEngineers;
		Cabines.total += (float)( Cabines.incomeSec * DBControl.main.elapsedTimeSinceLastCheck ) * Engineers.cabineEngineers;

		// Save calculated values for each module
		PlayerPrefs.SetFloat ( "Wheels" , Wheels.total );
		PlayerPrefs.SetFloat ( "Deposits" , Deposits.total );
		PlayerPrefs.SetFloat ( "Engines" , Engines.total );
		PlayerPrefs.SetFloat ( "Seats" , Seats.total );
		PlayerPrefs.SetFloat ( "Cabines" , Cabines.total );

		// Updates UI with values
		textWheels.text = Wheels.total.ToString ( "0.00") + " (" + 
			Wheels.incomeSec*60*60*Engineers.wheelsEngineers + "/h)";
		textDeposits.text = Deposits.total.ToString ( "0.00") + " (" + 
			Deposits.incomeSec*60*60*Engineers.depositsEngineers + "/h)";
		textEngines.text = Engines.total.ToString ( "0.00") + " (" + 
			Engines.incomeSec*60*60*Engineers.enginesEngineers + "/h)";
		textSeats.text = Seats.total.ToString ( "0.00") + " (" + 
			Deposits.incomeSec*60*60*Engineers.seatEngineers + "/h)";
		textCabines.text = Cabines.total.ToString ( "0.00") + " (" + 
			Cabines.incomeSec*60*60*Engineers.cabineEngineers + "/h)";

		yield return null;
		 */
	}
}
