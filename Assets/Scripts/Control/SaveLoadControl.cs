﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadControl : MonoBehaviour {

	public static SaveLoadControl main;
	public double now;
	public bool isNewGame, newGameReady;
	public bool ready = false;

	void Awake () {
		main = this;
		if ( !PlayerPrefs.HasKey ( "New Game" ) ) {
			isNewGame = true;
			Debug.Log ( "It is a new game. All data reset running..." );
			ResetData ( );
			PlayerPrefs.SetInt ( "New Game" , 1 );
			// StartCoroutine( DBControl.main.GetElapsedTime ( ) );
			// StartCoroutine ( DBControl.GetTime ( value => now = value ) );
			StartCoroutine ( SetNewGameTime ( ) );
		}
	}
	
	public IEnumerator SetNewGameTime ( ) {
		while ( DBControl.main.currentTimeStamp == 0 ) {
			yield return null;
		}
		PlayerPrefs.SetFloat ( "Last Time" , (float)DBControl.main.currentTimeStamp );
		newGameReady = true;
	}

	void Start ( ) {
		StartCoroutine ( LoadData ( ) );
	}

	public IEnumerator LoadData ( ) {

		while ( isNewGame && !newGameReady ) yield return null;			// Avoids loading while creating new game data
		while ( DBControl.main.currentTimeStamp == 0 ) yield return null;

		// Debug.Log ( "Now time sec is " + now );
		PlayerPrefs.SetFloat ( "Last Time" , (float)DBControl.main.currentTimeStamp );

		// Obtain saved resources on last connection
		/*ResourcesControl.main.rations.totalRaw = PlayerPrefs.GetFloat ( ResourcesControl.main.rations.resourceData.name );
		ResourcesControl.main.materials.totalRaw = PlayerPrefs.GetFloat ( ResourcesControl.main.materials.resourceData.name );
		ResourcesControl.main.steel.totalRaw = PlayerPrefs.GetFloat ( ResourcesControl.main.steel.resourceData.name );
		ResourcesControl.main.gold.totalRaw = PlayerPrefs.GetFloat ( ResourcesControl.main.gold.resourceData.name );
		ResourcesControl.main.population.totalRaw = PlayerPrefs.GetFloat ( ResourcesControl.main.population.resourceData.name ); */

		// Obtains saved population on last connection
		/*CharactersControl.main.idle.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.idle.charactersData.name );
		CharactersControl.main.workers.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.workers.charactersData.name );
		CharactersControl.main.officials.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.officials.charactersData.name );
		CharactersControl.main.engineers.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.engineers.charactersData.name );
		CharactersControl.main.soldiers.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.soldiers.charactersData.name );
		CharactersControl.main.pilots.totalRaw = PlayerPrefs.GetInt ( CharactersControl.main.pilots.charactersData.name ); */

		// Obtains assigned tasks for workers
		/* CitizensControl.main.idleWorkers = PlayerPrefs.GetInt ( "Idle Workers" );
		CitizensControl.main.rationsWorkers = PlayerPrefs.GetInt ( "Rations Workers" );
		CitizensControl.main.materialsWorkers = PlayerPrefs.GetInt ( "Materials Workers" );
		CitizensControl.main.steelWorkers = PlayerPrefs.GetInt ( "Steel Workers" );
		CitizensControl.main.buildingWorkers = PlayerPrefs.GetInt ( "Building Workers" );
		CitizensControl.main.academyWorkers = PlayerPrefs.GetInt ( "Academy Workers" ); */

		// Obtains assigned tasks for officials
		Officials.medkitsOfficials = PlayerPrefs.GetInt ( "Medkits Officials" );
		Officials.fuelOfficials = PlayerPrefs.GetInt ( "Fuel Officials" );
		Officials.ammoOfficials = PlayerPrefs.GetInt ( "Ammo Officials" );
		Officials.academyOfficials = PlayerPrefs.GetInt ( "Academy Officials" );

		// Obtains assigned tasks for engineers
		Engineers.wheelsEngineers = PlayerPrefs.GetInt ( "Wheels Engineers" );
		Engineers.depositsEngineers = PlayerPrefs.GetInt ( "Deposits Engineers" );
		Engineers.enginesEngineers = PlayerPrefs.GetInt ( "Engines Engineers" );
		Engineers.seatEngineers = PlayerPrefs.GetInt ( "Seats Engineers" );
		Engineers.cabineEngineers = PlayerPrefs.GetInt ( "Cabines Engineers" );
		Engineers.academyEngineers = PlayerPrefs.GetInt ( "Academy Engineers" );

		// Obtains assigned tasks for soldiers
		Soldiers.activeSoldiers = PlayerPrefs.GetInt ( "Active Soldiers" );
		Soldiers.academySoldiers = PlayerPrefs.GetInt ( "Academy Soldiers" );
		
		// Obtains consumable items
		Medkits.total = PlayerPrefs.GetFloat ( "Medkits" );
		Fuel.total = PlayerPrefs.GetFloat ( "Fuel" );
		Ammo.total = PlayerPrefs.GetFloat ( "Ammo" );

		// Obtains modules
		/*
		Wheels.total = PlayerPrefs.GetFloat ( "Wheels" );
		Deposits.total = PlayerPrefs.GetFloat ( "Deposits" );
		Engines.total = PlayerPrefs.GetFloat ( "Engines" );
		Seats.total = PlayerPrefs.GetFloat ( "Seats" );
		Cabines.total = PlayerPrefs.GetFloat ( "Cabines" );
		 */

		// TO DO: LOAD ALL BUILDING STATS
		ResidencesDB.level = PlayerPrefs.GetInt ( "Residences Level" );
		ResidencesDB.isUpgrading = PlayerPrefs.GetInt ( "Residences is Upgrading" ) == 1;
		ResidencesDB.progress = PlayerPrefs.GetFloat ( "Residences Progress" );

		FoodProductionPlantDB.level = PlayerPrefs.GetInt ( "Food Production Plant Level" );
		FoodProductionPlantDB.isUpgrading = PlayerPrefs.GetInt ( "Food Production Plant is Upgrading" ) == 1;
		FoodProductionPlantDB.progress = PlayerPrefs.GetFloat ( "Food Production Plant Progress" );

		RecyclingPlantDB.level = PlayerPrefs.GetInt ( "Recycling Plant Level" );
		RecyclingPlantDB.isUpgrading = PlayerPrefs.GetInt ( "Recycling Plant is Upgrading" ) == 1;
		RecyclingPlantDB.progress = PlayerPrefs.GetFloat ( "Recycling Plant Progress" );

		MetalWorksPlantDB.level = PlayerPrefs.GetInt ( "Metal Works Plant Level" );
		MetalWorksPlantDB.isUpgrading = PlayerPrefs.GetInt ( "Metal Works Plant is Upgrading" ) == 1;
		MetalWorksPlantDB.progress = PlayerPrefs.GetFloat ( "Metal Works Plant Progress" );

		WarehouseDB.level = PlayerPrefs.GetInt ( "Warehouse Plant Level" );
		WarehouseDB.isUpgrading = PlayerPrefs.GetInt ( "Warehouse Plant is Upgrading" ) == 1;
		WarehouseDB.progress = PlayerPrefs.GetFloat ( "Warehouse Plant Progress" );

		yield return null;

		ready = true;

	}

	public void SaveData ( ) {

	}

	public void ResetData ( ) {

		PlayerPrefs.DeleteAll ( );

		// Obtain saved resources on last connection
		/*PlayerPrefs.SetFloat ( ResourcesControl.main.rations.resourceData.name , 100 );
		PlayerPrefs.SetFloat ( ResourcesControl.main.materials.resourceData.name , 50  );
		PlayerPrefs.SetFloat ( ResourcesControl.main.steel.resourceData.name , 20 );
		PlayerPrefs.SetFloat ( ResourcesControl.main.gold.resourceData.name , 10 );
		PlayerPrefs.SetFloat ( ResourcesControl.main.population.resourceData.name , 4 ); */

		// Obtains saved population on last connection
		PlayerPrefs.SetInt ( CharactersControl.main.idle.charactersData.name , 0 );
		PlayerPrefs.SetInt ( CharactersControl.main.workers.charactersData.name , 4 );
		PlayerPrefs.SetInt ( CharactersControl.main.officials.charactersData.name , 0 );
		PlayerPrefs.SetInt ( CharactersControl.main.engineers.charactersData.name , 0 );
		PlayerPrefs.SetInt ( CharactersControl.main.soldiers.charactersData.name , 0 );
		PlayerPrefs.SetInt ( CharactersControl.main.pilots.charactersData.name , 0 );

		// Obtains assigned tasks for workers
		PlayerPrefs.SetInt ( "Idle Workers" , 0 );
		PlayerPrefs.SetInt ( "Rations Workers" , 1 );
		PlayerPrefs.SetInt ( "Materials Workers" , 1 );
		PlayerPrefs.SetInt ( "Steel Workers" , 1 );
		PlayerPrefs.SetInt ( "Building Workers" , 1 );
		PlayerPrefs.SetInt ( "Academy Workers" , 0 );

		// TO DO: LOAD ALL BUILDING STATS...
		PlayerPrefs.SetInt ( "Residences Level" , 1 );
		PlayerPrefs.SetInt ( "Food Production Plant Level" , 1 );
		PlayerPrefs.SetInt ( "Recycling Plant Level" , 1 );
		PlayerPrefs.SetInt ( "Metal Works Plant Level" , 1 );
		PlayerPrefs.SetInt ( "Warehouse Plant Level" , 1 );
		// ...

	}

}
