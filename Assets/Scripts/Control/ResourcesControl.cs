﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RoadBastards;

public class ResourcesControl : MonoBehaviour {

	public static ResourcesControl main;

	[SerializeField]
	public List<ResourceData> resourcesData;
	public List<Resource> resources;

	public List<ResourceUI> resourcesUI;
	public List<ResourceProductionUI> resourcesProductionsUI;

	void Awake ( ) {
		main = this;
	}

	void Start ( ) {
		// Here must calculate disconnected income
		int index = 0;
		foreach ( ResourceUI current in resourcesUI ) {
			float amount = GetResourceAmount ( resources[index].resourceData );
			current.UpdateUI ( resources[index].resourceData.name , resources[index].resourceData.sprite , 
					(int)amount , (int)( ( amount - (int)amount ) * 100 ) ,
					(int)BuildingsControl.main.GetStorageLimit ( resources[index].resourceData ) );
			index++;
		}
		index = 0;
		foreach ( ResourceProductionUI current in resourcesProductionsUI ) {
			current.UpdateUI ( resources [ index ] );
			index++;
		}
	}

	void Update ( ) {
		int index = 0;
		foreach ( Resource current in resources ) {
			resourcesUI [ index ].UpdateUI ( current.resourceData.name , current.resourceData.sprite , 
				current.total , (int)( current.progress * 100 ) , 
				(int)BuildingsControl.main.GetStorageLimit ( current.resourceData ) );
			index++;
		}
		index = 0;
		foreach ( ResourceProductionUI current in resourcesProductionsUI ) {
			current.UpdateUI ( resources [ index ] );
			index++;
		}
	}

	// Methods to add resources directly, applies the warehouse limit
	public void AddResource ( ResourceData resource , float amount ) {
		int index = 0;
		foreach ( Resource current in resources ) {
			if ( current.resourceData == resource ) {
				current.totalRaw += amount;
				current.totalRaw = Mathf.Clamp ( current.totalRaw , 
					0 , BuildingsControl.main.GetStorageLimit ( resource ) );
				/* resourcesUI [ index ].UpdateUI ( resource.name , resource.sprite , 
					current.total , (int)( current.progress * 100 ) , 
					(int)BuildingsControl.main.GetStorageLimit ( resource ) ); */
				return;
			}
			index++;
		}
	}
	public void AddResource ( ResourceAmount resourceAmount ) {
		AddResource ( resourceAmount.resource , resourceAmount.amount );
	}
	public void AddResourceBundle ( ResourceBundle resourceBundle ) {
		foreach ( ResourceAmount current in resourceBundle.resources ) {
			AddResource ( current );
		}
	}

	// Methods to add resources based on a income
	public void AddResourceIncome ( ResourceData resource , float income , float citizens , float time ) {
		AddResource ( resource , ( income * citizens * time ) / 3600f );
	}
	public void AddResourceIncome ( ResourceIncome resourceIncome , float citizens , float time ) {
		AddResourceIncome ( resourceIncome.resource , resourceIncome.income , citizens , time );
	}
	public void AddResourceIncomeBundle ( ResourceIncomeBundle resourceIncomeBundle , float citizens , float time ) {
		foreach ( ResourceIncome current in resourceIncomeBundle.resourceIncomes ) {
			AddResourceIncome ( current , citizens , time );
		}
	}

	public float GetResourceAmount ( ResourceData resource ) {
		foreach ( Resource current in resources ) {
			if ( current.resourceData == resource ) {
				return current.totalRaw;
			}
		}
		return 0;
	}

	public IEnumerator CalculateDisconnectedIncome ( ) {

		while ( DBControl.main.busy ) yield return null;		// Waits DB response
		while ( !SaveLoadControl.main.ready ) yield return null;		// Waits Load data response

		// If is NOT a new game, then must calculate incoming while disconnected time
		// if ( !SaveLoadControl.main.isNewGame ) {
			// Calculates incoming for each resource, limiting to the warehouse and residence limits

			// rations.totalRaw += (float)( rations.resourceData.baseProductionPerWorker * DBControl.main.elapsedTimeSinceLastCheck ) * Workers.rationsWorkers;
			/* rations.totalRaw = Mathf.Clamp ( rations.totalRaw , 0 , WarehouseDB.maxRations );
			
			// materials.totalRaw += (float)( materials.resourceData.baseProductionPerWorker * DBControl.main.elapsedTimeSinceLastCheck ) * Workers.materialsWorkers;
			materials.totalRaw = Mathf.Clamp ( materials.totalRaw , 0 , WarehouseDB.maxMaterials );

			// steel.totalRaw += (float)( steel.resourceData.baseProductionPerWorker * DBControl.main.elapsedTimeSinceLastCheck ) * Workers.steelWorkers;
			steel.totalRaw = Mathf.Clamp ( steel.totalRaw , 0 , WarehouseDB.maxSteel );

			// gold.totalRaw += (float)( gold.resourceData.baseProductionPerWorker * DBControl.main.elapsedTimeSinceLastCheck ) * Workers.total;

			population.totalRaw += (float)( 1f/24f/3600f * DBControl.main.elapsedTimeSinceLastCheck );
			population.totalRaw = Mathf.Clamp ( population.totalRaw , 0 , ResidencesDB.maxCitizens );
		}

		// Save new calculated values for each resource (IT COULD BE A METHOD OF SaveLoadControl.cs)
		PlayerPrefs.SetFloat ( rations.resourceData.name , rations.totalRaw );
		PlayerPrefs.SetFloat ( materials.resourceData.name , materials.totalRaw );
		PlayerPrefs.SetFloat ( steel.resourceData.name , steel.totalRaw );
		PlayerPrefs.SetFloat ( gold.resourceData.name , gold.totalRaw );
		PlayerPrefs.SetFloat ( population.resourceData.name , population.totalRaw );
		PlayerPrefs.SetFloat ( "Last Time" , (float)DBControl.main.currentTimeStamp ); */
		
		// Updates UI and show totals and incomes for each resource

		yield return null;

	}


}
