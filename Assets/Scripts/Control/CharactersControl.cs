﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharactersControl : MonoBehaviour {

	public static CharactersControl main;
	[SerializeField]
	public Characters idle , workers , officials , engineers , soldiers , pilots;

	// This is for the always visible UI of population
	public Text textWorkers, textOfficials, textEngineers, textSoldiers, textPilots;

	void Awake ( ) {
		main = this;
	}

	void Start ( ) {
		InvokeRepeating ( "ProcessIncome" , 1f , 1f );	// NOT NEED CAUSE REAL INCOME IS PROCESSED AT START BY DB
		InvokeRepeating ( "UpdateUI" , 1f , 1f );
		StartCoroutine ( CalculateDisconnectedIncome ( ) );
	}

	public IEnumerator CalculateDisconnectedIncome ( ) {

		while ( !SaveLoadControl.main.ready ) yield return null;		// Wait for data load

		Workers.rationsWorkers = PlayerPrefs.GetInt ( "Rations Workers" );
		Workers.materialsWorkers = PlayerPrefs.GetInt ( "Materials Workers" );
		Workers.steelWorkers = PlayerPrefs.GetInt ( "Steel Workers" );

		// Population has been calculated at ResourcesControl, but here new population is
		//  assigned always as workers in buildings
		int oldPopulation = PlayerPrefs.GetInt ( "Workers" );
		int newPopulation = (int)Population.total - PlayerPrefs.GetInt ( "Workers" );
		PlayerPrefs.SetInt ( "Workers" , oldPopulation+newPopulation );
		Workers.total = oldPopulation+newPopulation;
		PlayerPrefs.SetInt ( "Building Workers" , PlayerPrefs.GetInt ( "Building Workers" )+newPopulation );
		Workers.buildingWorkers = PlayerPrefs.GetInt ( "Building Workers" )+newPopulation;

		yield return null;

	}

	void ProcessIncome ( ) {
		// Not possible to estimate income... all are int values, not decimals
	}

	void UpdateUI ( ) {
		textWorkers.text = workers.totalRaw.ToString ( );
		textOfficials.text = officials.totalRaw.ToString ( );
		textEngineers.text = engineers.totalRaw.ToString ( );
		textSoldiers.text = soldiers.totalRaw.ToString ( );
		textPilots.text = pilots.totalRaw.ToString ( );
	}

}
