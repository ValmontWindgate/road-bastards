﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasControl : MonoBehaviour {

	public static CanvasControl main;
	public enum TpCanvas { Debug , Fade , MainMenu , Resources , Production , 
        ResourcesProduction , Buildings ,
        ADDWHATYOUWANT }	    // All Canvas access list
	public List<Canvas> canvas;

    public Camera camera;
    public RenderTexture cameraRenderTexture;
    public RawImage cameraImageBig, cameraImageSmall;
    public Texture cameraTexture;
	
	public bool debugOn = true;	// Debug Canvas elements
	public Text debugConsoleText, debugText;
	
	public float fading = 0f;	// Fade Canvas elements
	public Image fadeImage;

	void Awake ( ) {
		if ( main == null ) main = this;
	}

	// Muestra, oculta o invierte el estado de ciertos canvas
    public void Show ( TpCanvas target , bool show = true ) {
        canvas[(int)target].enabled = show;
    }
    public void Switch ( TpCanvas target ) {
        canvas[(int)target].enabled = !canvas[(int)target].enabled;
    }
    public void ShowAll ( bool show = true ) {		// Este método afecta a TODOS
        foreach (Canvas current in canvas){
            current.enabled = show;
        }
        if (debugOn) Show ( TpCanvas.Debug );
    }

    public void SwitchDebug ( ) {
        Switch ( TpCanvas.Debug );
    }

    public void ShowMenu ( int index ) {
        for ( int i=(int)TpCanvas.Resources; i<=(int)TpCanvas.Buildings; i++ ){
            Show ( (TpCanvas)i , i==index );
        }
    }

    public void ShowIntro ( ) {
        Show ( TpCanvas.Resources , false );
        Show ( TpCanvas.Production , false );
        Show ( TpCanvas.ResourcesProduction , false );
        Show ( TpCanvas.Buildings , false );
    }

    public void CameraFullScreen ( bool on = true ) {
        if ( on ) {
            // camera.targetTexture = null;
            cameraImageSmall.texture =cameraTexture;
            cameraImageBig.texture = cameraRenderTexture;
        }
        else{
            // camera.targetTexture = cameraRenderTexture;
            cameraImageBig.texture = cameraTexture;
            cameraImageSmall.texture = cameraRenderTexture;
        } 
    }

	// Permite mostrar información en el canvas de debug del ejecutable
	public void DebugLog ( string text ) {
		debugConsoleText.text += "\n" + text;
        Debug.Log ( text );
	}

	// (COROUTINE) Fade screen to color/alpha using the fade canvas image
    public void Fade ( Color color , float time = 0.5f) {
        CanvasControl.main.DebugLog("Canvas Control fading screen to " + color + "..." );
        fading = 0f;
        StartCoroutine(FadeCoroutine( color, time));
    }
    public void Fade ( float alpha , float time = 0.5f ) {
        Fade ( new Color(0,0,0,alpha) , time );
    }
    IEnumerator FadeCoroutine ( Color color , float time = 0.5f) {
        Color startColor = fadeImage.color;
        float timer = 0f;
        while ( timer < time ) {
            timer += Time.deltaTime;
            fading = timer / time;
            fading = Mathf.Clamp(fading, 0f, 1f);
            fadeImage.color = new Color ( 
                Mathf.Lerp(startColor.r , color.r , fading) , 
                Mathf.Lerp(startColor.g , color.g , fading) , 
                Mathf.Lerp(startColor.b , color.b , fading) , 
                Mathf.Lerp(startColor.a , color.a , fading) );
            yield return null;
        }
        CanvasControl.main.DebugLog("Canvas control finished screen fade to " + color + "!" );
        fading = 1f;
    }
    public bool IsFading ( ) {
        return fading < 1f;
    }

}
