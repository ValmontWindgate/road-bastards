﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CitizensControl : MonoBehaviour {
   
   public static CitizensControl main;

   public List<CitizenData> citizensData;
   public List<int> citizens;
   public List<int> idleCitizens;

   public int debugAllCitizens;

   void Awake ( ) {
       if ( main == null ) main = this;
   }

    void Start ( ) {
        // citizens = new List<int> ( citizensData.Count );
    }

    // Gets total of citizens of a given rank
    public int GetCitizens ( CitizenData citizenData ) {
        return (int)ResourcesControl.main.GetResourceAmount ( citizenData as ResourceData );
    }
    // Gets the amount of citizens of a given rank that are idle (Not assigned to production)
    public int GetIdleCitizens ( CitizenData citizenData ) {
        int index = 0;
        foreach ( CitizenData current in citizensData ) {
            if ( current == citizenData ) {
                return idleCitizens [ index ];
            }
            index++;
        }
        return 0;
    }

    public int GetAllCitizens ( ) {
        int all = 0;
        int index = 0;
        foreach ( CitizenData current in citizensData ) {
            all += citizens [ index ];
            index++;
        }
        return all;
    }

   void Update ( ) {

       // Recalculate active and idle citizens of each rank
       int index = 0;
       foreach ( CitizenData current in citizensData ) {
           citizens [ index ] = GetCitizens ( current );
           idleCitizens [ index ] = citizens [ index ] - BuildingsControl.main.GetActiveCitizens ( current );
           index++;
       }

       debugAllCitizens = GetAllCitizens ( );

   }


}
