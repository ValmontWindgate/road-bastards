﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RewardsControl : MonoBehaviour {

	// Add 1 new worker to population
	public void GetWorker ( ) { GetWorkerStatic ( ); }
	public static void GetWorkerStatic ( ) {

		// Add 1 worker to scripts
		Population.total += 1f;
		Population.totalWorkers += 1;	// Population has Workers.total replicated...
		Workers.total += 1;
		Workers.buildingWorkers += 1f;

		// Add 1 workers to saved data
		PlayerPrefs.SetFloat ( "Population" , PlayerPrefs.GetFloat ( "Population" )+1f );
		PlayerPrefs.SetInt ( "Workers" , PlayerPrefs.GetInt ( "Workers" )+1 );
		PlayerPrefs.SetInt ( "Building Workers" , PlayerPrefs.GetInt ( "Building Workers" )+1 );

		Debug.Log ( "You get 1 free worker!" );

	}

	public void Exit ( ) {
		SceneManager.LoadScene ( 0 );
	}
	
}
