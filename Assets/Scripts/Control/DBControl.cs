﻿// Methods to calculate time from DB
using System;
using System.Collections;
using UnityEngine;

public class DBControl : MonoBehaviour {

	public static DBControl main;
	public bool busy = true;
	public static DateTime currentDateTime;
	public double lastTimeStamp = 0, currentTimeStamp = 0;
	public double elapsedTimeSinceLastCheck = 0 , elapsedTimeSinceLastCheckInSec = 0 , elapsedTimeSinceLastCheckInHours = 0;

	void Awake ( ) {
		main = this;
	}

	// Gets elapsed time for first time
	IEnumerator Start ( ) {
		yield return StartCoroutine ( CalculateElapsedTimeSinceLastCheckCoroutine ( ) );
		busy = false;
	}

	// Calculates elapsed time since last checking by comparing current DB time with local saved last time. It must be called at start, and every
	//  time is need to check resources and upgrades. Also must be called on quit!
	public IEnumerator CalculateElapsedTimeSinceLastCheckCoroutine ( ) {

		// Load saved time and get current time from DB, quit game if not connection
		lastTimeStamp = PlayerPrefs.GetFloat ( "Last Time" );
		yield return StartCoroutine ( GetTime ( value => currentTimeStamp = value ) );
		if ( currentTimeStamp == 0f ) {
			Debug.LogError ( "NO INTERNET CONNECTION, UNABLE TO GET TIME FROM DATABASE, GAME QUIT WITH FATAL ERROR!!!" );
			Application.Quit ( );
		}

		// Calculates the difference in seconds
		elapsedTimeSinceLastCheck = currentTimeStamp - lastTimeStamp;
		elapsedTimeSinceLastCheckInSec = elapsedTimeSinceLastCheck/1000f;
		elapsedTimeSinceLastCheckInHours = elapsedTimeSinceLastCheckInSec/3600f;
		CanvasControl.main.DebugLog ( "Last connection time " + lastTimeStamp + "ms and now time is " + currentTimeStamp + "ms, time elapsed " + elapsedTimeSinceLastCheck + "ms");

		// Updates saved last time
		PlayerPrefs.SetFloat ( "Last Time" , (float)currentTimeStamp );
		CanvasControl.main.DebugLog ( "In seconds since last connection " + elapsedTimeSinceLastCheckInSec + ", in hours " + elapsedTimeSinceLastCheckInHours );
	}

	public static IEnumerator GetTime (System.Action<double> resultado){

		string url = "https://trinit.es/alberto/getTime.php";

		DateTime ahora;
		DateTime referencia = new DateTime(2019, 1, 1, 0, 0, 0);

		// Hacemos la llamada a la url y obtenemos el resultado
		WWW hs_get = new WWW(url);
		yield return hs_get;
		if(hs_get.error != null){
			Debug.Log("ERROR: "+hs_get.error);
			resultado (0);
		} else {

			// Recuperamos la fecha
			ahora = DateTime.Parse (hs_get.text);
			currentDateTime = ahora;
			print (ahora);

			// Calculamos el tiempo entre las dos fechas
			TimeSpan tiempo = ahora - referencia;

			resultado(tiempo.TotalMilliseconds);
		}

	}

}
