﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

public class BuildingsControl : MonoBehaviour {

	public static BuildingsControl main;

	[SerializeField]
	public List<StorageBuilding> storageBuildings;
	public ResidencesBuilding residencesBuilding;
	public List<ProductionBuilding> productionBuildings;

	public List<ProductionUI> productionsUI;
	public List<BuildingUI> buildingsUI;

	public int debugNumberOfBuildingsUpgrading;

	void Awake ( ) {
		main = this;
	}

	void Start ( ) {
		int index = 0;
		// Update of production UI
		foreach ( ProductionUI current in productionsUI ) {
			float income = 0;
			ProductionBuildingData productionBuildingData = productionBuildings[index].buildingData as ProductionBuildingData;
			if ( productionBuildingData.activeIncomePerCitizen.resourceIncomes.Count > 0 ) {
				income = (int)productionBuildings[index].GetResourceIncome ( 
					productionBuildingData.activeIncomePerCitizen.resourceIncomes[0].resource );
			}
			current.UpdateUI ( productionBuildings[index].buildingData.name ,
				productionBuildings[index].buildingData.sprite , 
				(int)income ,
				productionBuildings[index].citizens ,
				productionBuildings[index].maxCitizens );
			index++;
		}
		// Update of building UI
		index = 0;
		foreach ( StorageBuilding current in storageBuildings ) {
			buildingsUI[index].UpdateUI ( current );
			index++;
		}
		buildingsUI[index].UpdateUI ( residencesBuilding );
		index++;
		foreach ( ProductionBuilding current in productionBuildings ) {
			buildingsUI[index].UpdateUI ( current );
			index++;
		}
	}
	
	// Get storage limit for resources based on its storage buildings, if no storage
	//  building exits, then the limit is infinity (i.e gold)
	public float GetStorageLimit ( ResourceData resource ) {
		StorageBuildingData storageBuildingData = null;
		foreach ( StorageBuilding current in storageBuildings ) {
			storageBuildingData = current.buildingData as StorageBuildingData;
			if ( storageBuildingData.resource == resource ) {
				return current.level * storageBuildingData.storagePerLevel;
			}
		}
		if ( resource == residencesBuilding.citizensData [ 0 ] ) {
			return residencesBuilding.maxCitizens;
		}
		return 1000000000;
	}

	// Get the assigned citizens of a given rank to production buildings
	public int GetActiveCitizens ( CitizenData citizen ) {
		int total = 0;
		foreach ( ProductionBuilding current in productionBuildings ) {
			ProductionBuildingData productionBuildingData = current.buildingData as ProductionBuildingData;
			if ( productionBuildingData.requiredCitizen == citizen ) {
				total += current.citizens;
			}
		}
		return total;
	}
	public int GetActiveCitizensInProduction ( ProductionBuildingData productionBuilding ) {
		foreach ( ProductionBuilding current in productionBuildings ) {
			if ( current.buildingData == productionBuilding ) {
				return current.citizens;
			}
		}
		return 0;
	}
	// Search over all production buildings, return the one who produces an active income of the type
	//  of the given resource data
	public ProductionBuilding GetBuildingForResource ( ResourceData resourceData ) {
		foreach ( ProductionBuilding currentProductionBuilding in productionBuildings ) {
			ProductionBuildingData productionBuildingData = currentProductionBuilding.buildingData as ProductionBuildingData;
			foreach ( ResourceIncome currentResourceIncome in 
				productionBuildingData.activeIncomePerCitizen.resourceIncomes ) {
				if ( currentResourceIncome.resource == resourceData ) {
					return currentProductionBuilding;
				}
			}
		}
		return null;
	}
	public ProductionBuilding GetBuildingBuilding ( ) {
		return productionBuildings [ 3 ];
	}
	public int GetActiveCitizensInBuilding ( ) {
		return GetActiveCitizensInProduction ( productionBuildings[3].buildingData as ProductionBuildingData );
	}
	public int GetActiveCitizensInResource ( ResourceData resourceData ) {
		// Search for the building/s (Must be one...) that produces that resource, and returns its assigned citizens
		foreach ( ProductionBuilding current in productionBuildings ) {
			if ( current.GetResourceIncome ( resourceData ) > 0 ) {
				return current.citizens;
			}
		}
		// If resource is gold, active citizens are all workers (Also includes whose whit negative gold income...)
		if ( resourceData == ResourcesControl.main.resourcesData [ 0 ] ) {
			return CitizensControl.main.GetAllCitizens ( );
		}
		// If resource is a citizen, count all citizens of its rank
		foreach ( CitizenData citizenData in CitizensControl.main.citizensData ) {
			if ( resourceData == citizenData ) {
				return CitizensControl.main.GetCitizens ( citizenData );
			}
		}
		return 0;
	}

	public float GetGlobalProductionIncome ( ResourceData resourceData ) {
		int index = 0;
		float income = 0;
		foreach ( ProductionBuilding currentProductionBuilding in productionBuildings ) {
			ProductionBuildingData productionBuildingData = currentProductionBuilding.buildingData as ProductionBuildingData;
			foreach ( ResourceIncome currentResourceIncome in
				productionBuildingData.activeIncomePerCitizen.resourceIncomes ) {
				if ( currentResourceIncome.resource == resourceData ) {
					income += currentProductionBuilding.citizens * currentResourceIncome.income ;
				}
			}
		}
		return income;
	}

	// Set an amount of workers to a building, checking if it is possible, changing citizen from idle
	//  and updating UI
	public void SetActiveCitizensToProduction ( ProductionBuildingData productionBuilding , int amount ) {
		int currentActiveCitizens = 0;
		int maxActiveCitizens = 0;
		int neededIdleCitizens = 0;
		ProductionBuilding targetBuilding = null;
		// Checks building and its currently active citizens
		foreach ( ProductionBuilding current in productionBuildings ) {
			if ( current.buildingData == productionBuilding ) {
				targetBuilding = current;
				currentActiveCitizens = current.citizens;
				maxActiveCitizens = current.maxCitizens;
				neededIdleCitizens = amount - currentActiveCitizens;
				Debug.LogFormat ( "Try to change production of {0} to {1}" , 
					targetBuilding.buildingData.name , amount );
				Debug.LogFormat ( "Current {0}, Max {1}, Need {2}" , 
					currentActiveCitizens,maxActiveCitizens,neededIdleCitizens);
			}
		}
		int currentIdleCitizens = 0;
		int index = 0;
		// Check idle citizens of the type required for the building
		foreach ( CitizenData current in CitizensControl.main.citizensData ) {
			if ( current == productionBuilding.requiredCitizen ) {
				currentIdleCitizens = CitizensControl.main.idleCitizens [ index ];
				Debug.LogFormat ( "Found {0} idle citizens" , currentIdleCitizens );
				// If there is enough idle citizens to assing, and it don't overlap limit
				if ( amount <= currentIdleCitizens + currentActiveCitizens ) {
					if ( amount <= maxActiveCitizens ) {
						targetBuilding.citizens = amount;
						CitizensControl.main.idleCitizens [ index ] -= neededIdleCitizens;
						Debug.LogFormat ( "Production changed succesfuly" );
						float income = 0;
						ProductionBuildingData productionBuildingData = targetBuilding.buildingData as ProductionBuildingData;
						if ( productionBuildingData.activeIncomePerCitizen.resourceIncomes.Count > 0 ) {
							income = (int)targetBuilding.GetResourceIncome ( 
								productionBuildingData.activeIncomePerCitizen.resourceIncomes[0].resource );
						}
						productionsUI [ index ].UpdateUI ( targetBuilding.buildingData.name ,
							targetBuilding.buildingData.sprite , 
							(int)income ,
							targetBuilding.citizens ,
							targetBuilding.maxCitizens );
				
					}

				}
				break;
			}
			index++;
		}
		
	}

	void Update ( ) {

		// Debug and testing
		if ( Input.GetKeyDown ( KeyCode.Alpha1 ) ) {
			SetActiveCitizensToProduction ( productionBuildings[0].buildingData as ProductionBuildingData , 5 );
		}
		if ( Input.GetKeyDown ( KeyCode.Alpha2 ) ) {
			ProductionBuildingData productionBuildingData = productionBuildings[0].buildingData as ProductionBuildingData;
			Debug.LogFormat ( "Total income of {0} {1}" , productionBuildingData.name ,
				GetGlobalProductionIncome ( 
					productionBuildingData.activeIncomePerCitizen.resourceIncomes[0].resource ) );
		}
		debugNumberOfBuildingsUpgrading = Building.numberOfBuildingsUpgrading;

		// Update of production UI
		int index = 0;
		foreach ( ProductionUI current in productionsUI ) {
			float income = 0;
			ProductionBuildingData productionBuildingData = productionBuildings[index].buildingData as ProductionBuildingData;
			if ( productionBuildingData.activeIncomePerCitizen.resourceIncomes.Count > 0 ) {
				income = (int)productionBuildings[index].GetResourceIncome ( 
					productionBuildingData.activeIncomePerCitizen.resourceIncomes[0].resource );
			}
			current.UpdateUI ( productionBuildings[index].buildingData.name ,
				productionBuildings[index].buildingData.sprite , 
				(int)income ,
				productionBuildings[index].citizens ,
				productionBuildings[index].maxCitizens );
			index++;
		}

		// Update UI building level upgrades
		index = 0;
		foreach ( StorageBuilding current in storageBuildings ) {
			buildingsUI[index].UpdateUI ( current );
			index++;
		}
		buildingsUI[index].UpdateUI ( residencesBuilding );
		index++;
		foreach ( ProductionBuilding current in productionBuildings ) {
			buildingsUI[index].UpdateUI ( current );
			index++;
		}

	}

}
