﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConsumablesControl : MonoBehaviour {

	public static ConsumablesControl main;

	// This is for the always visible UI of resources
	public Text textMedkits, textFuel, textAmmo;

	void Awake ( ) {
		main = this;
	}

	void Start ( ) {
		// InvokeRepeating ( "ProcessIncome" , 1f , 1f );	// NOT USED CAUSE INCOME IS PROCESSED ONLY AT START
		StartCoroutine ( CalculateDisconnectedIncome ( ) );
	}

	public IEnumerator CalculateDisconnectedIncome ( ) {

		while ( DBControl.main.elapsedTimeSinceLastCheckInSec == 0 ) yield return null;		// Waits DB response

		// Calculates incoming for each consumable
		Medkits.total += (float)( Medkits.incomeSec * DBControl.main.elapsedTimeSinceLastCheckInSec ) * Officials.medkitsOfficials;
		Fuel.total += (float)( Fuel.incomeSec * DBControl.main.elapsedTimeSinceLastCheckInSec ) * Officials.fuelOfficials;
		Ammo.total += (float)( Ammo.incomeSec * DBControl.main.elapsedTimeSinceLastCheckInSec ) * Officials.ammoOfficials;

		// Save calculated values for each resource
		PlayerPrefs.SetFloat ( "Medkits" , Medkits.total );
		PlayerPrefs.SetFloat ( "Fuel" , Fuel.total );
		PlayerPrefs.SetFloat ( "Ammo" , Ammo.total );

		// Updates UI and show totals and incomes for each resource
		textMedkits.text = Medkits.total.ToString ( "0.00") + " (" + 
			Medkits.incomeSec*60*60*Officials.medkitsOfficials + "/h)";
		textFuel.text = Fuel.total.ToString ( "0.00") + " (" + 
			Fuel.incomeSec*60*60*Officials.fuelOfficials + "/h)";
		textAmmo.text = Ammo.total.ToString ( "0.00") + " (" + 
			Ammo.incomeSec*60*60*Officials.ammoOfficials + "/h)";

		yield return null;

	}

	// This could calculate resources aproximately without checking real DB time
	/* public void ProcessIncome ( ) {
		Medkits.total += Medkits.incomeSec;
		Fuel.total += Fuel.incomeSec;
		Ammo.total += Ammo.incomeSec;
	} */

}
