﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

public class ProductionBuilding : Building
{

    private ProductionBuildingData productionBuildingData;
    public int citizens;

    public int maxCitizens { get { 
        ProductionBuildingData productionBuildingData = buildingData as ProductionBuildingData;
        return level*productionBuildingData.citizenSlotsPerLevel; 
    } }
    public int debugMaxCitizens;
    public List<float> debugIncomes;

    void Start ( ) {
        productionBuildingData = buildingData as ProductionBuildingData;
        InvokeRepeating ( "UpdateProduction" , 1f , 1f );
    }

    void UpdateProduction ( ) {

        // Check and update level upgrades
        UpgradeLevelProgress ( 1f );

        // Calculate incoming resources
        ResourcesControl.main.AddResourceIncomeBundle ( productionBuildingData.activeIncomePerCitizen , citizens , 1f );
        debugMaxCitizens = maxCitizens;

        // Refresh incomes for each resource for this building
        int index = 0;
        foreach ( Resource current in ResourcesControl.main.resources ) {
            debugIncomes [ index ] = GetResourceIncome ( current.resourceData );
            index++;
        }

    }

    // Get the income for a given resource, only for this building
    public float GetResourceIncome ( ResourceData resourceData ) {
        foreach ( ResourceIncome current in productionBuildingData.activeIncomePerCitizen.resourceIncomes ) {
            if ( current.resource == resourceData ) {
                return citizens * current.income;
            }
        }
        return 0f;
    }

}
