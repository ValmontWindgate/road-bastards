﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

public class ResidencesBuilding : Building
{
    public List<CitizenData> citizensData;

    public int maxCitizens { get { 
        ResidenceBuildingData residenceBuildingData = buildingData as ResidenceBuildingData;
        return level*residenceBuildingData.roomPerLevel; } }
    public int debugMaxCitizens;

    void Start ( ) {
        InvokeRepeating ( "UpdateProduction" , 1 , 1 );
    }

    void UpdateProduction ( ) {

        UpgradeLevelProgress ( 1f );

        // Process passive income for each kind of citizen (gold inc/dec and rations dec)
        foreach ( CitizenData current in citizensData ) {
            // Debug.Log ( "Nº " + current.name + " " + ResourcesControl.main.GetResourceAmount ( current as ResourceData ));
            ResourcesControl.main.AddResourceIncomeBundle ( current.passiveIncomePerUnit , 
                ResourcesControl.main.GetResourceAmount ( current as ResourceData ) , 1f );
        }
        // Process passive income of new workers 1 each hour
        ResourcesControl.main.AddResourceIncome ( citizensData [ 0 ] as ResourceData , 1 , 1 , 1f );
        debugMaxCitizens = maxCitizens;
    }
}
