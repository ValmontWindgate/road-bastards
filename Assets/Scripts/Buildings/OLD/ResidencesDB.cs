﻿[System.Serializable]
public class ResidencesDB : BuildingDB {

	public static string name = "Residences";

	// Propertie with the max citizens that residences can store
	public static int maxCitizens {
		get { return level * 20; }
	}

	// Current citizens and empty slots
	public static int currentCitizens;
	public static int vacancy;

}
