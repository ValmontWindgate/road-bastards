﻿using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

[System.Serializable]
public class BuildingDB {

    public BuildingType type; 

	// Name, level, max level and cost increase per level
	public static string name;
	[SerializeField]
	public static int level = 1;
	public static int levelMax = 20;
	public static float incCostPerLevel = 0.1f;
	public static bool isUpgrading;
	public static float progress;
	
	// People working inside generating resources, or working as builders improving the level of the building
	public int workers, builders;
	
	// Base construction cost in hours with 1 working builder
	public float hoursCostBase = 1f;
	public float hoursCost {
		get { return hoursCostBase + hoursCostBase * incCostPerLevel * level; }
		set { }
	}

	// Base resources cost
	public float rationsCostBase = 400f , materialsCostBase = 300f , steelCostBase = 200f , goldCostBase = 100f;
	public float rationsCost {
		get { return rationsCostBase + rationsCostBase * incCostPerLevel * level; }
		set { }
	}
	public float materialsCost {
		get { return materialsCostBase + materialsCostBase * incCostPerLevel * level; }
		set { }
	}
	public float steelCost {
		get { return steelCostBase + steelCostBase * incCostPerLevel * level; }
		set { }
	}
	public float goldCost {
		get { return goldCostBase + goldCostBase * incCostPerLevel * level; }
		set { }
	}
	
	// WORK IN PROGRESS class with the list of improves made to a bulding when level up, such as
	//  enable/disable parts, change material, etc
	[System.Serializable]
	public class BuildingLevelUp {
		public enum Type { Enable , Disable , Replace , Move , LightColor, Material };
		public List<Type> types;
		public List<GameObject> gameObjects;
		public List<float> heights;
		public List<Color> lightColors;
		public List<Material> materials;
	}

	public List<BuildingLevelUp> buildingLevelUp;

}
