﻿public class WarehouseDB : BuildingDB {

	public static string name = "Warehouse";

	// Max storage for each resource
	public static int maxRations {
		get { return level * 400; }
	}
	
	public static int maxMaterials {
		get { return level * 300; }
	}

	public static int maxSteel {
		get { return level * 200; }
	}

}