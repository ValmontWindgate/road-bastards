﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu ( menuName = "Road Bastards/Buildings/Residences Building") ]
public class ResidenceBuildingData : BuildingData {
    public List<CitizenData> storedCitizens;
    public int roomPerLevel;
}
