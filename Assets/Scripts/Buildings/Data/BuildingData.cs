﻿using UnityEngine;
using System.Collections.Generic;
using RoadBastards;

[CreateAssetMenu ( menuName = "Road Bastards/Buildings/Base Building") ]
public class BuildingData : ScriptableObject {

	public string name , description;					// Name of the resource
	public Sprite sprite;								// UI and 3D aspect
	public GameObject prefab;

	public int maxLevel;								// Leveling and costs
	public ResourceBundle baseCost;
	public float buildingTimeCost;
	public float costIncreasePerLevel;

	public VisualImproveLevels visualImproveLevels;		// Visual changes made to the building when leveling

	/* public BuildingType type;
	public BuildingRangeType range;
					
	[SerializeField]
	public List<ResourceAmount> basePrize;
	public float baseBuildingTimeInHours;
	public float baseBuildingTime { get { return baseBuildingTimeInHours * 3600f;} }
	public float perLevelncrease; */

	// Will be helpful to have a list of visual improves for building levels...

}
