﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu ( menuName = "Road Bastards/Buildings/Storage Building") ]
public class StorageBuildingData : BuildingData {
    public ResourceData resource;
    public int storagePerLevel;       // i.e steel warehouse store 1000/level
}
