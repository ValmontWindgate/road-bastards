﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

public class AcademyBuildingData : BuildingData
{
    // Is only used to spend resources (and a citizen) and get a citizen as a result, but this
    //  kind of building could work with any kind of resources...
    public float triningTime;
    public ResourceBundle trainingCost;
    public ResourceBundle trainingResult;

}
