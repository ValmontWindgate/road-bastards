﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RoadBastards;

[CreateAssetMenu ( menuName = "Road Bastards/Buildings/Production Building") ]
public class ProductionBuildingData : BuildingData {
   public CitizenData requiredCitizen;
   public int citizenSlotsPerLevel;
   public ResourceIncomeBundle activeIncomePerCitizen;
}