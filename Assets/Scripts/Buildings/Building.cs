﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{

    public BuildingData buildingData;
    public int level = 1;
    public bool isUpgrading = false;
    public float upgradeProgress = 0f;

    public static int numberOfBuildingsUpgrading;

    // Calculate upgrade progress for a time, updating variables for level upgrading state
    public void UpgradeLevelProgress ( float time ) {
        if ( !isUpgrading ) return;
        if ( upgradeProgress == 0f ) numberOfBuildingsUpgrading += 1;
        // Building base class must know about BuildingData
        upgradeProgress += time * 
            BuildingsControl.main.GetActiveCitizensInBuilding ( ) / 
            ( buildingData.buildingTimeCost * 3600f * numberOfBuildingsUpgrading );
        if ( upgradeProgress >= 1f ) {
            isUpgrading = false;
            upgradeProgress = 0f;
            numberOfBuildingsUpgrading -= 1;
        }
    }

    public float GetUpgradeRemainingTime ( ) {
        return ( buildingData.buildingTimeCost * numberOfBuildingsUpgrading ) / BuildingsControl.main.GetActiveCitizensInBuilding ( );
    }
   
}
