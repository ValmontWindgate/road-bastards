﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingUI : MonoBehaviour
{
    public Text nameText;
    public Text descriptionText;
    public Image spriteImage;

    public GameObject costPanel;
    public List<Text> costTexts;
    public List<Image> costImages;

    public Button upgradeButton;

    public GameObject progressPanel;
    public Text progressText;
    public Slider slider;
    public Text sliderText;

    public void UpdateUI ( Building building ) {
        nameText.text = building.buildingData.name;
        descriptionText.text = building.buildingData.description;
        spriteImage.sprite = building.buildingData.sprite;

        if ( !building.isUpgrading ) {
            costPanel.SetActive ( true );
            upgradeButton.interactable = true;
            upgradeButton.onClick.RemoveAllListeners ( );
            upgradeButton.onClick.AddListener ( ( ) => UpgradeButton ( building ) );
            progressPanel.SetActive ( false );
            for ( int i = 0 ; i < building.buildingData.baseCost.resources.Count ; i ++ ) {
                costTexts [ i ].text = building.buildingData.baseCost.resources [ i ].amount *
                    (1f + building.level * building.buildingData.costIncreasePerLevel ) + "";
                costImages [ i ].sprite = building.buildingData.baseCost.resources [ i ].resource.sprite;
            }
        }
        else {
            costPanel.SetActive ( false );
            upgradeButton.interactable = false;
            progressPanel.SetActive ( true );
            progressText.text = "Progress " + (int)(building.upgradeProgress*100) + "%";
            slider.value = building.upgradeProgress;
            sliderText.text = building.GetUpgradeRemainingTime ( ) + "h";
        }

    }

    public void UpgradeButton ( Building building ) {
        // Check if there is enought resources
        for ( int i = 0 ; i < building.buildingData.baseCost.resources.Count ; i ++ ) {
            float required = building.buildingData.baseCost.resources [ i ].amount *
                    ( 1f + building.level * building.buildingData.costIncreasePerLevel );
            if ( ResourcesControl.main.GetResourceAmount ( 
                building.buildingData.baseCost.resources [ i ].resource ) < required ) {
                Debug.LogFormat ( "Not enough resources {0}" , 
                    building.buildingData.baseCost.resources [ i ].resource.name );
                return;
            }
        }
        // If we get this point, we have enough resorces, go removing 
        for ( int i = 0 ; i < building.buildingData.baseCost.resources.Count ; i ++ ) {
            float required = building.buildingData.baseCost.resources [ i ].amount *
                    ( 1f + building.level * building.buildingData.costIncreasePerLevel );
            ResourcesControl.main.AddResource ( 
                building.buildingData.baseCost.resources [ i ].resource , -required );
        }
        // Start upgrading
        building.isUpgrading = true;
    }

}
