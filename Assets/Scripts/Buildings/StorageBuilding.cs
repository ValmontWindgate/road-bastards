﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageBuilding : Building
{

    public int max { get { 
        StorageBuildingData storageBuildingData = buildingData as StorageBuildingData;
        return level*storageBuildingData.storagePerLevel; 
    } }
    public int debugMax;

    void Start ( ) {
        InvokeRepeating ( "UpdateProduction" , 1f , 1f );
    }

    public void UpdateProduction ( ) {
        debugMax = max;
        UpgradeLevelProgress ( 1f );
    }
    
}
