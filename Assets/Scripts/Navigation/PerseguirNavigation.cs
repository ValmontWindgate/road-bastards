using UnityEngine;
using System.Collections;

public class PerseguirNavigation : MonoBehaviour {
	
	public GameObject destino;
	private UnityEngine.AI.NavMeshAgent agenteDeNavegacion;

	void Start ( ) {
		agenteDeNavegacion = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent> ( );
	}

	void Update ( ) {
		agenteDeNavegacion.SetDestination ( destino.transform.position );
	}

}
