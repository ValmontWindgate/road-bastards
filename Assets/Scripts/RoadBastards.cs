﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RoadBastards {
    
    // Resources and citizens, buildings and vehicles enum types  
    public enum ResourceType { 
        Ration , Material , Steel , Gold , Population ,
        Fuel , Ammo , Medkit ,
        Wheel , Engine , Deposit , Seat , Cabine ,
        Worker , Official , Engineer , Soldier , Pilot
    };

    public enum ResourceRangeType { Basic , Consumable , Module , Citizen };

    public enum CitizenRangeType { Worker , Official , Engineer , Soldier , Pilot };

    public enum WorkType { 
        Idle , CitizenProduction , RationProduction , MaterialProduction , SteelProduction , Building ,
        TrainingOfficial , TrainingEngineer , TrainingSoldier , TrainingPilot ,
        FuelProduction , AmmoProduction , MedkitProduction ,
        WheelProduction , EngineProduction , DepositProduction , 
        SeatProduction , CabineProduction , All
    };

    public enum BuildingRangeType { Storage , Training , BasicProduction , ConsumableProduction , ModuleProduction ,     }

    public enum BuildingType { 
        Residence , Warehouse , FoodPlant , RecyclingPlant , MetalPlant , 
        Academy ,
		FuelPlant , ArmamentPlant , MedicalPlant , 
        WheelFactory , EngineFactory , DepositFactory , 
		SeatFactory , CabineFactory
    };

    // Visual improve classes to set building changes along leveling
    public enum VisualImproveType { Enable , Disable , Color , Move , Scale };

    [System.Serializable]
    public class VisualImprove {
        public VisualImproveType type;
        public Color color;
        public Vector3 move;
        public Vector3 scale;
    }

    public class VisualImproveLevels {
        [SerializeField]
        public List<VisualImprove> levels;
    }

    // Resource grouping classes to set costs and incomes of many kind of resources
    [System.Serializable]
    public class ResourceAmount {
        public ResourceData resource;
        public int amount;
    }

    [System.Serializable]
    public class ResourceIncome {
        public ResourceData resource;
        public float income;
    }

    [System.Serializable]
    public class ResourceBundle {
        [SerializeField]
        public List<ResourceAmount> resources;
    }

    [System.Serializable]
    public class ResourceIncomeBundle {
        [SerializeField]
        public List<ResourceIncome> resourceIncomes;
    }

    public enum ModuleType { Wheel , Engine , Deposit , Seat , Cabine };

}