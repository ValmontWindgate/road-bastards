﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomDesdeLista : MonoBehaviour {

	public int maxValor = 10;
	public int randQuestion;
	public List<int> valores;
	public bool fin;

	void Start ( ) {

		valores = new List<int>( );					// Inicializamos la nueva lista de números

		for ( int i = 0 ; i < maxValor ; i++ ) {	// Le añadimos los valores desde 0 hasta el máximo
			valores.Add ( i );
		}
	}

	// Update is called once per frame
	void Update ( ) {

		if ( Input.GetKeyDown ( KeyCode.P ) && fin == false ) {

			int elemento = Random.Range ( 0 , valores.Count );		// Elegimos una posición aleatoria de la lista

			randQuestion = valores [ elemento ];					// Obtenemos el número que guardaba

			valores.RemoveAt ( elemento );							// Eliminamos ese número de la lista

			if ( valores.Count == 0 ) fin = true;					// Si ya hemos consumido la lista finalizamos

			Debug.Log ("EL VALOR RANDQUESTION ACTUAL ES " + "  " + randQuestion);

		}

	}
		
}	


