﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
	
	public GameObject player;
	public bool active = false;
	public float coolDown = 1f;

	// Update is called once per frame
	void Update () {

		coolDown -= Time.deltaTime;	// Cooldown para poder subir/bajar del coche

		if ( active == true ) {

			transform.Translate ( Vector3.forward * Input.GetAxis ( "Vertical" ) * Time.deltaTime );	
			transform.Rotate ( Vector3.up * Input.GetAxis ( "Horizontal" ) * 180 * Time.deltaTime );

			// Activa el personaje y desactiva el control del coche
			if ( Input.GetKeyDown ( KeyCode.E ) && coolDown <= 0 ) {
				Debug.Log ( "Jugador baja del coche" );
				active = false;
				player.SetActive ( true );
				player.transform.position = transform.position + transform.right * 2;
				coolDown = 1f;
			}

		}

	}

	// Si el personaje está cerca del coche y pulsa E se desactiva el personaje y se activa el control del coche
	void OnTriggerStay ( ) {
		if ( Input.GetKeyDown ( KeyCode.E ) && active == false && coolDown <= 0 ) {
			Debug.Log ( "Jugador sube al coche" );
			active = true;
			player.SetActive ( false );
			coolDown = 1f;
		}
	}

}
