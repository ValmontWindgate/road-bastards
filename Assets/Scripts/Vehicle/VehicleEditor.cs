﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleEditor : MonoBehaviour {

	private Vector3 newObjectPosition = new Vector3 ( 5 , 0 , 0 );		// Position where new modules are created, must be near of the vehicle

	public List<GameObject> modulePrefabs;

	public GameObject cursor;
	public Cursor cursorScript;
	public Vector3 cursorPosition = new Vector3 ( 0 , 1 , 0 );
	public List<Material> cursorMaterials;

	public bool isModuleSelected;
	public bool isNewModule;
	public GameObject cursorSelectedModule;
	public ModuleDB cursorSelectedModuleDB;

	public VehicleDB vehicleData;
	public Dictionary<Vector3,GameObject> vehicleGameObjects;
	
	void Start ( ) {
		cursorScript = FindObjectOfType<Cursor>( );
		CreateVehicleBase ( );
	}

	void Update () {
		if ( CheckCreateInput ( ) ) return;		// Check if user wants to create new module and stop processing this frame if true
		CursorMove ( );							// Check Input and moves cursor between editor blocks
		CursorSelection ( );					// Change cursor mode and selection of modules
	}

	void CreateVehicleBase ( ) {
		vehicleData = new VehicleDB ( );
		vehicleGameObjects = new Dictionary<Vector3, GameObject>( );
		vehicleData.AddModule ( new Vector3 ( 0 , 1 , 0 ) , new PilotDB ( ) );
		vehicleGameObjects.Add ( new Vector3 ( 0 , 1 , 0 ) , Instantiate ( modulePrefabs [ 0 ] , new Vector3 ( 0 , 1 , 0 ) , Quaternion.identity ) );
		vehicleData.AddModule ( new Vector3 ( 0 , 0 , 0 ) , new WheelDB ( ) );
		vehicleGameObjects.Add ( new Vector3 ( 0 , 0 , 0 ) , Instantiate ( modulePrefabs [ 1 ] , new Vector3 ( 0 , 0 , 0 ) , Quaternion.identity ) );
		vehicleData.AddModule ( new Vector3 ( 1 , 0 , 0 ) , new WheelDB ( ) );
		vehicleGameObjects.Add ( new Vector3 ( 1 , 0 , 0 ) , Instantiate ( modulePrefabs [ 1 ] , new Vector3 ( 1 , 0 , 0 ) , Quaternion.identity ) );
		vehicleData.AddModule ( new Vector3 ( 0 , 0 , -1 ) , new WheelDB ( ) );
		vehicleGameObjects.Add ( new Vector3 ( 0 , 0 , -1 ) , Instantiate ( modulePrefabs [ 1 ] , new Vector3 ( 0 , 0 , -1 ) , Quaternion.identity ) );
		vehicleData.AddModule ( new Vector3 ( 1 , 0 , -1 ) , new WheelDB ( ) );
		vehicleGameObjects.Add ( new Vector3 ( 1 , 0 , -1 ) , Instantiate ( modulePrefabs [ 1 ] , new Vector3 ( 1 , 0 , -1 ) , Quaternion.identity ) );
		vehicleData.Refresh ( );
	}

	bool CheckCreateInput ( ) {
		bool isCreating = false;
		if ( Input.GetKeyDown ( KeyCode.Alpha1 ) || Input.GetKeyDown ( KeyCode.Alpha2 ) || Input.GetKeyDown ( KeyCode.Alpha3 ) || Input.GetKeyDown ( KeyCode.Alpha4 ) ) {
			cursorScript.SetPosition ( newObjectPosition );
			isCreating = true;
			if ( isNewModule && isModuleSelected ) {			// If there is already a new module, destroy it before creating next module
				DestroyModule ( cursorScript.transform.position );
			}
			isNewModule = true; isModuleSelected = true;
			cursorScript.ColorTint = Cursor.Mode.Selection;
			cursorScript.ShowSelectionCursor ( true );
		}
		if ( Input.GetKeyDown ( KeyCode.Alpha1 ) ) {			// Wheel creation
			CreateModule ( ModuleDB.Type.Wheel , newObjectPosition );
		}
		else if ( Input.GetKeyDown ( KeyCode.Alpha2 ) ) {		// Engine creation
			CreateModule ( ModuleDB.Type.Engine , newObjectPosition );
		}
		else if ( Input.GetKeyDown ( KeyCode.Alpha3 ) ) {		// Deposit creation
			CreateModule ( ModuleDB.Type.Deposit , newObjectPosition );
		}
		else if ( Input.GetKeyDown ( KeyCode.Alpha4 ) ) {		// Seat creation
			CreateModule ( ModuleDB.Type.Seat , newObjectPosition );
		}
		return isCreating;
	}

	void CreateModule ( ModuleDB.Type type , Vector3 position ) {
		cursorSelectedModule = Instantiate ( modulePrefabs [ (int)type ] , position , Quaternion.identity );
		vehicleGameObjects.Add ( position , cursorSelectedModule );
		switch ( type ) {
			case ModuleDB.Type.Wheel : vehicleData.AddModule ( position , new WheelDB ( ) ); break;
			case ModuleDB.Type.Engine : vehicleData.AddModule ( position , new EngineDB ( ) ); break;
			case ModuleDB.Type.Deposit : vehicleData.AddModule ( position , new DepositDB ( ) ); break;
			case ModuleDB.Type.Seat : vehicleData.AddModule ( position , new SeatDB ( ) ); break;
			default : break;
		}
		
	}

	void DestroyModule ( Vector3 position ) {
		if ( vehicleGameObjects.ContainsKey ( position ) ) {
			Destroy ( vehicleGameObjects [ position ] );
		}
		vehicleGameObjects.Remove ( position );
		vehicleData.modules.Remove ( position );
	}

	bool ExistsModule ( Vector3 position ) {
		return vehicleData.modules.ContainsKey ( position ) && vehicleGameObjects.ContainsKey ( position );
	}

	void CursorMove ( ) {

		// Avoids processing another move when cursor is already moving
		if ( cursorScript.isMoving ) return;

		// Calculates desired next position for the cursor by checking Input, if there is no Input, returns
		Vector3 move = Vector3.zero;
		if ( Input.GetKeyDown ( KeyCode.A ) ) move = Vector3.back;
		else if ( Input.GetKeyDown ( KeyCode.D ) ) move = Vector3.forward;
		else if ( Input.GetKeyDown ( KeyCode.W ) ) move = Vector3.left;
		else if ( Input.GetKeyDown ( KeyCode.S ) ) move = Vector3.right;
		else if ( Input.GetKeyDown ( KeyCode.LeftControl ) ) move = Vector3.down;
		else if ( Input.GetKeyDown ( KeyCode.LeftShift ) ) move = Vector3.up;
		if ( move == Vector3.zero ) return;

		// Cursor normal move without any selection
		cursorScript.isMoving = true;
		StartCoroutine ( cursorScript.Move ( move ) );
		
		/*if ( !isModuleSelected && Input.GetMouseButtonDown ( 0 ) && vehicleData.modules.ContainsKey ( cursorPosition ) && vehicleGameObjects.ContainsKey ( cursorPosition ) ) {
			isModuleSelected = true;
			cursorSelectedModuleDB = vehicleData.modules [ cursorPosition ];
			cursorSelectedModule = vehicleGameObjects [ cursorPosition ];
		}
		else if ( Input.GetMouseButtonDown ( 0 ) ) {
			isModuleSelected = false;
		}
		if ( isModuleSelected && vehicleData.modules.ContainsKey ( cursorPosition ) ) {
			vehicleData.SwapModule ( cursorLastPosition , cursorPosition );
			
			cursor.transform.position = cursorPosition;
		}
		else if ( !isModuleSelected ) {
			cursor.transform.position = cursorPosition;
		}*/
	}

	void CursorSelection ( ) {

		if ( cursorScript.isMoving) return;						// Avoids processing while cursor is moving

		// It there is a clic, and there is a module where cursor is, then change selection
		if ( Input.GetMouseButtonDown ( 0 ) ) {
			if ( cursorScript.ColorTint == Cursor.Mode.Move ) {					// If cursor is in move mode, tries to select the module at its position
				if ( ExistsModule ( cursorScript.transform.position ) ) {
					cursorSelectedModule = vehicleGameObjects [ cursorScript.transform.position ];
					cursorSelectedModuleDB = vehicleData.modules [ cursorScript.transform.position ];
					cursorScript.ShowSelectionCursor ( true );
					cursorScript.ColorTint = Cursor.Mode.Selection;
				}
			}
			else if ( cursorScript.ColorTint == Cursor.Mode.Selection ) {		// If cursor is in selection mode, tries to move selected module to its position				
				// If there is a module at destiny, swap modules!
				if ( ExistsModule ( cursorScript.transform.position ) ) {
					SwapModule ( cursorScript.transform.position , cursorScript.cursorSelectionObject.transform.position );
					cursorScript.ShowSelectionCursor ( false );
					cursorScript.ColorTint = Cursor.Mode.Move;
				}
				// Otherwise moves the module to the new position and delete from where it was before!
				else {
					vehicleData.modules.Add ( cursorScript.transform.position , cursorSelectedModuleDB );
					vehicleData.modules.Remove ( cursorScript.cursorSelectionObject.transform.position );
					vehicleGameObjects.Add ( cursorScript.transform.position , cursorSelectedModule );
					vehicleGameObjects.Remove ( cursorScript.cursorSelectionObject.transform.position );
					cursorSelectedModule.transform.position = cursorScript.transform.position;
					cursorScript.ShowSelectionCursor ( false );
					cursorScript.ColorTint = Cursor.Mode.Move;
				}
			}
		}

	}

	void SwapModule ( Vector3 positionA , Vector3 positionB ) {
		Debug.Log ( "Swapping modules between " + positionA + positionB );
		vehicleData.SwapModule ( cursorScript.transform.position , cursorScript.cursorSelectionObject.transform.position );
		GameObject tempGameObject = vehicleGameObjects [ cursorScript.transform.position ];
		vehicleGameObjects [ cursorScript.transform.position ] = vehicleGameObjects [ cursorScript.cursorSelectionObject.transform.position ];
		vehicleGameObjects [ cursorScript.cursorSelectionObject.transform.position ] = tempGameObject;
		Vector3 tempPosition = vehicleGameObjects [ cursorScript.transform.position ].transform.position;
		vehicleGameObjects [ cursorScript.transform.position ].transform.position = vehicleGameObjects [ cursorScript.cursorSelectionObject.transform.position ].transform.position;
		vehicleGameObjects [ cursorScript.cursorSelectionObject.transform.position ].transform.position = tempPosition;
	}

}
