﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VehicleDB {

    public Dictionary<Vector3,ModuleDB> modules;
    public int xMax = 2 , yMax = 2 , zMax = 2; 
    
    public List<WheelDB> wheels;
    public List<EngineDB> engines;
    public List<DepositDB> deposits;

    public PilotDB pilot;
    public List<SeatDB> seats;

    public VehicleDB ( ) {
        modules = new Dictionary<Vector3, ModuleDB>( );
        wheels = new List<WheelDB>( );
        engines = new List<EngineDB>( );
        deposits = new List<DepositDB>( );
        pilot = new PilotDB( );
        seats = new List<SeatDB>( );
    }

    // Refresh by reading ALL the vehicle and re-assigning all lists (Engines, Deposits...)
    public void Refresh ( ) {
        wheels = new List<WheelDB>( );
        engines = new List<EngineDB>( );
        deposits = new List<DepositDB>( );
        pilot = new PilotDB ( );
        seats = new List<SeatDB>( );
        foreach ( ModuleDB current in this.modules.Values ) {
            switch ( current.type ) {
                case ModuleDB.Type.Wheel : 
                    wheels.Add ( (WheelDB)current );
                    break;
                case ModuleDB.Type.Engine : 
                    engines.Add ( (EngineDB)current );
                    break;
                case ModuleDB.Type.Deposit : 
                    deposits.Add ( (DepositDB)current );
                    break;
                case ModuleDB.Type.Pilot : 
                    pilot = (PilotDB)current;
                    break;
                case ModuleDB.Type.Seat : 
                    seats.Add ( (SeatDB)current );
                    break;
                default : 
                    Debug.Log ("HALO");
                    break;
            }
        }
    }
    
    // Get the total weight of the vehicle, by adding the weight of all the modules
    public float TotalWeight {
        get { 
            float total = 0;
            foreach ( ModuleDB current in this.modules.Values ) {
                total += current.weight;
            }
            return total;
        }
    }

    public float TotalPower {
        get {
            float total = 0;
            foreach ( EngineDB current in this.engines ) {
                total += current.power;
            }
            return total;
        }
    }

    public float TotalFuel {
        get {
            float total = 0;
            foreach ( DepositDB current in this.deposits ) {
                total += current.fuel;
            }
            return total;
        }
    }

    // Determinates which is the max gear for this vehicle total power and weight
    public int MaxGear {
        get { return (int)(this.TotalPower/this.TotalWeight); } 
        set { this.MaxGear = value; }
    }

    public void AddModule ( Vector3 position , ModuleDB module ) {
        modules.Add ( position , module );
    }

    public void SwapModule ( Vector3 positionA , Vector3 positionB ) {
        if ( !modules.ContainsKey ( positionA ) || !modules.ContainsKey ( positionB ) ) return;
        ModuleDB temp = new ModuleDB ( );
        temp = modules [ positionA ];
        modules [ positionA ] = modules [ positionB ];
        modules [ positionA ] = temp;
    }

}
