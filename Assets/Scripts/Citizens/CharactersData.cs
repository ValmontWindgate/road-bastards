﻿using UnityEngine;
using System.Collections.Generic;
using RoadBastards;

[CreateAssetMenu ( menuName = "Road Bastards/Characters") ]
public class CharactersData : ScriptableObject {

	public string name;						// Name of the resource
	public CitizenRangeType type;
	public Sprite sprite;					// Sprite (UI) and model (3D) por graphic representation
	public GameObject model;
			
    public int academyLevelRequired;
    public CharactersData characterRequired;

	[SerializeField]
	public List<ResourceAmount> prize;
	public float trainingTimeInHours;
	public float trainingTime { get { return trainingTimeInHours * 3600f;} }

	// Will be helpful to have a list of visual improves for building levels...

}
